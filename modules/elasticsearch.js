var dotenv = require('dotenv');
dotenv.load();

//Configurations
var host  = process.env.ELASTIC_HOST || 'localhost:9200';
var indexName = process.env.ELASTIC_INDEX || 'nuage_sample'; 
var type = process.env.ELASTIC_TYPE || 'nuage_doc_type'; 
var templateName = process.env.ELASTIC_TEMPLATE || 'nuage_sample'; 

var elasticsearch = require('elasticsearch');

var elasticClient = new elasticsearch.Client({  
  host: host,
  log: 'info'
});

/**
 * Method to delete a template
 */
function deleteTemplate() {
  return elasticClient.indices.deleteTemplate({
    name: templateName
  });
}
exports.deleteTemplate = deleteTemplate;

/**
 * Method to create a template
 * @argument {json} body json for template
 */
function createTemplate(body) {
   return elasticClient.indices.putTemplate({
    name: templateName,
    body: body
  });
}
exports.createTemplate = createTemplate;


/**
 * Checking Whether template exists
 */
function templateExists() {
  return elasticClient.indices.existsTemplate({
    name: templateName
  });
}
exports.templateExists = templateExists;


/**
* Delete an existing index
*/
function deleteIndex () {  
  return elasticClient.indices.delete({
    index: indexName
  });
}
exports.deleteIndex = deleteIndex;

/**
* create the index
*/
function initIndex () {  
  return elasticClient.indices.create({
    index: indexName
  });
}
exports.initIndex = initIndex;

/**
* To check if the index exists
*/
function indexExists () {  
  return elasticClient.indices.exists({
    index: indexName
  });
}
exports.indexExists = indexExists;

/**
 * To add a document
 */
function addDocument(document) {  
  return elasticClient.index({
    index: indexName,
    type: type,
    body: {
      name: document.name,
      location: document.location,
      timestamp: document.timestamp
    }
  });
}
exports.addDocument = addDocument;

/**
 * To reterive all documents
 */
function getDocuments() {  
  return elasticClient.search({
    index: indexName,
    type: type,
    size: 240,
    body: {
      query: {
        match_all: {}
      },
      sort: [
        {timestamp: 'asc'}
      ]
    }
  });
}
exports.getDocuments = getDocuments;