# README #
This project is showing a sample demo of HeatMap using D3, Elasticsearch and NodeJS (Express JS).

Running demo can be found [here](http://69.60.115.13:8010).

### How to set it up? ###
* Firstly install NodeJS in your local system or server and then clone this repository.
* From the root folder of your project, do run following commands to install dependencies:
```
#!javascript
npm install
bower install
copy .env.example to .env and put the values for respective options
```

Note: For Elastic Search - You have two options:

* Either set it up locally or at some server
* Setup AWS Elasticsearch Service.

and then put respective connection URL in "**ELASTIC_HOST**" option in **.env** file.

Thats all to set up the project.

###Create template, index and load sample data###
I have created a script "**load-data.js**" in "**scripts**" folder, which will basically create the template, index, and then load data into that created index with specific template.

Please note, everytime you run this it will firstly delete the existing template, indexes, and documents, if exists and then recreate them.

To execute the script, run following command:

```
#!javascript

node scripts/load-data.js
```
You see output like:

```
#!node

Checking Template Exists...
Deleting Template...
Creating Template...
Checking Index Exists...
Deleting Index...
Creating Index...
Loading Data...

```



###Run the server###
To start the server you can either use pm2 to run the script index.js or following command:

```
#!node
node index.js

```

If all goes well, then it will start the server at your provided HOST and PORT defined as options under "**.env**"

###API's###
Following routes have been defined in express js:

* /location/get-data

  To reterive all the values from the elastic search sorted by **timestamp**

* /location/download-data

  To download the CSV of all the data from elasticsearch sorted by **timestamp**