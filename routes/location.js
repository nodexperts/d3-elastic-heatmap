// ROUTES FOR OUR API
var express = require('express');
var router = express.Router();
var elastic = require('../modules/elasticsearch');
var json2csv = require('json2csv');

// middleware to use for all requests
router.use(function (req, res, next) {
    next(); // make sure we go to the next routes and don't stop here
});

/**
 * POST API to get manual keyword results from scraper
 */
router.get('/get-data', function (req, res) {
  elastic.getDocuments().then(function (response) {  
    res.send(filterRecords(response));
  });
});

router.get('/download-data', function (req, res) {
  elastic.getDocuments().then(function (response) {  
    json2csv({ data: filterRecords(response), fields: ['name', 'timestamp', 'location'], fieldNames: ['Name', 'Timestamp', 'Location']}, function(err, csv) {
      res.setHeader('Content-disposition', 'attachment; filename=data.csv');
      res.set('Content-Type', 'text/csv');
      res.status(200).send(csv);
    });
  });
});

function filterRecords(response) {
  var finalResponse = [];
  if(typeof response.hits.hits !== 'undefined') {
    finalResponse = (response.hits.hits).map(function(i) {
      return i._source;
    });
  }
  
  return finalResponse;
}

module.exports = router;