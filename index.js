var dotenv = require('dotenv');
dotenv.load();

//Configurations
var port = process.env.PORT || 8000;  
var host = process.env.HOST || 'localhost';  // set our port

var express = require('express');
var app         = express();

var bodyParser  = require('body-parser');
var locationRoutes = require('./routes/location.js');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public')); 
app.use('/location', locationRoutes);

app.get('*', function(req, res) {
  res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

// START THE SERVER
app.listen(port, host);
console.log('API running at: ' + host + ':' + port);
