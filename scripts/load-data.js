/**
 * 
 * Scripts will check for template, if exists, then delete it and then recreate it. 
 * After creating the template, it will look for index, if exists, then it will delete it and then recreate it.
 * After creating the index we will push the dummy documents into it.
 */

var elastic = require('../modules/elasticsearch');
var moment = require('moment');

var locations = ['Grandma Prisbrey\'s Bottle Village', 'The Stagecoach Inn', 'Bodega Harbour', 'Buena Vista Winery',
    'Sebastiani Vineyards and Winery', 'Sonoma Plaza', 'Big Basin Redwoods State Park', 'The Santa Cruz Beach Boardwalk',
    'The Felton Covered Bridge', 'Mission San Diego', 'Marshall Gold Discovery State Historic Park', 'The Old Custom House', 
    'Hearst Castle', 'Jack London State Historic Park', 'Petrified Forest', 'The HP Garage', 'Alcatraz Island',
    'Yosemite Valley', 'Governor\'s Mansion State Historic Park', 'Conservatory of Flowers', 'The Bok Kai Temple', 
    'Golden Gate Bridge', 'Presidio of Santa Barbara', 'Presidio of San Francisco', 'Telegraph Hill', 'Treasure Island',
    'The Lasky-DeMille Barn', 'Christmas Tree Lane', 'Union Square', 'Paramount Theatre', 'Avila Adobe', 'Mark Hopkins Hotel',
    'Lone Mountain', 'Ebbetts Pass', 'Angel Island', 'Emigrant Gap', 'State Capitol', 'Marin Civic Center', 
    'California\'s First Theatre', 'Beringer Vineyards', 'Cresta Blanca Winery', 'China Camp State Park', 'Charles Krug Winery',
    'Schramsberg Vineyards', 'The Beach Boys Historic Landmark', 'Manhattan Beach Pier', 'Joaquin Miller House', 'Point Sur Lighthouse',
    'Crocker Art Museum', 'Old Sacramento State Historic Park'
];

var personNames = ['Sir Isaac Newton', 'Leonardo di ser Piero da Vinci', 'William Shakespeare', 'Adolf Hitler', 'Abraham',
    'Paul the Apostle of Tarsus', 'Siddhartha Gautama', 'Moses', 'Muḥammad', 'Jesus of Nazareth'];

console.log('Checking Template Exists...');
elastic.templateExists().then(function(exists) {
  if(!exists) {
    return createTemplate();
  } else {
    console.log('Deleting Template...');
    elastic.deleteTemplate().then(function() {
      return createTemplate();
    });
  }
});
 
function createTemplate() {
  console.log('Creating Template...');
  var body = {
    order: 0,
    template: "nuage_*",
    settings: {},
    mappings: {
      nuage_doc_type: {
        dynamic_templates: [{
          strings: {
            mapping: {
              index: "not_analyzed",
              type: "string"
            },
            match_mapping_type: "string"
          }
        }, 
        {
          timestamps: {
            mapping: {
              type: "date",
              format: "yyyy-MM-dd HH:mm:ss"
            },
            match: "timestamp"
          }
        }]
      }
    },
    aliases: {}
  };
  elastic.createTemplate(body).then(function() {
    return indexExists();
  });
}
 
 
function indexExists() {
  console.log('Checking Index Exists...');
  elastic.indexExists().then(function(exists) {
    if(!exists) {
      return createIndex();
    } else {
      console.log('Deleting Index...');
      elastic.deleteIndex().then(function() {
        return createIndex();
      });
    }
  });
}

function createIndex() {
  console.log('Creating Index...');
  elastic.initIndex().then(function() {
    return loadData();
  });
}

function loadData() {
  console.log('Loading Data...');
  for(var time = 0; time < 24; time++) {
    var timestamp = moment().startOf('day').add(time, 'hour').format('YYYY-MM-DD HH:mm:ss');
    for(var nameKey in personNames) {
      var data = {
        timestamp: timestamp,
        name: personNames[nameKey],
        location: locations[Math.floor(Math.random() * 49)]
      };
      elastic.addDocument(data).then(function(response) {
      }, function (response) {
        console.log('Failed while adding document');  
      });
    }
  }
}