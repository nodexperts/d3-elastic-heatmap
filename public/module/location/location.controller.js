angular.module('HeatmapElastic', ['location-heatmap']).
  controller('locationCtrl', function ($scope, $http) {
    var vm = this;
    vm.scope = $scope;
    
    vm.loader = {
      data: false,
      download: false
    };
    
    vm.data = {
      location: null
    };
    
    vm.getData = getData;
    vm.downloadData = downloadData;
    
    function getData () {
      vm.loader.data = true;
      $http({method: 'GET', url: '/location/get-data'}).
        then(function(response) {
          vm.loader.data = false;  
          if(response.status === 200) {
            vm.data.location = response.data;
          } else {
            vm.data.location = [];
          }
        });
    }
    
    function downloadData () {
        vm.loader.download = true;
        
        $http({method: 'GET', url: '/location/download-data'}).
          then(function(response) {
            vm.loader.download = false;
            
            if(response.status !== 200)
              return;
          
            var anchor = angular.element('<a/>');
            anchor.attr({
              href: 'data:attachment/csv;charset=utf-8,' + encodeURI(response.data),
              target: '_blank',
              download: 'locations.csv'
            })[0].click();
            
          });
    }
});