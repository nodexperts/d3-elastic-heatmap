'use strict';

/* globals d3 */
angular.module('location-heatmap', []).
  directive('locationHeatmap', ['$window', function ($window) {
    return {
    restrict: 'E',
    scope: {
      data: '=',
      color: '=?',
      overview: '=?',
      handler: '=?'
    },
    replace: true,
    template: '<div class="location-heatmap"></div>',
    link: function (scope, element) {
      // Initialize svg element
      var dataInitialized = false;
      var padding = {top: 100, right: 30, bottom: 100, left: 100};
      var dimensions = {
        width: 1000,
        height: 200,
        gridSize: 0
      };

      var ordinals = {
        x: null,
        y: null,
        color: null
      };

      var domains = {
        x: null,
        y: null
      };

      var tooltipOptions = {
        transition_duration: 500,
        in_transition: false
      };

      var svg = d3.select(element[0])
        .append('svg')
        .attr('class', 'svg');

      var gContainer = svg.append("g")
        .attr("transform", "translate(" + padding.left + "," + padding.top + ")");

      // Add tooltip to the same element as main svg
      var tooltip = d3.select(element[0]).append('div')
        .attr('class', 'location-tooltip')
        .style('opacity', 0);

      angular.element($window).bind('resize', function () {
        scope.$apply();
      });

      // Watch for resizing and call drawChart Method
      scope.$watch(function () {
        return element[0].clientWidth;
      }, function ( w ) {
        if ( !w ) { return; }
        dimensions.width = (w < 300 ? 300 : w) - padding.left - padding.right;
        dimensions.gridSize = Math.floor(dimensions.width / 24);

        if ( !!scope.data && !!dataInitialized ) {
          scope.drawChart();
        }
      });

      // Watch for data availability and call drawChart Method
      scope.$watch('data', function (data) {
        //scope.finalData = [];
        if ( !data ) { return; }

        scope.setColorOrdinal();
        dataInitialized = true;

        scope.setXDomain();
        scope.setYDomain();

        scope.drawChart();
      });

      /*
       * Reteriving domain of Y Axis
       */
      scope.setYDomain = function () {
        domains.y = d3.map(scope.data, function(d){return d.name;}).keys().sort();
      };

      /*
       * Reteriving domain of X Axis
       */
      scope.setXDomain = function () {
        domains.x = d3.map(scope.data, function(d){return d.timestamp;}).keys().sort();
      };

      /*
       * Generating and seting Ordinal Colors on the basis of distint locations
       */
      scope.setColorOrdinal = function () {
        //Fetching Locations
        var locations = d3.map(scope.data, function(d){return d.location;}).keys().sort();

        //Creating Custom Colors for locations
        var colors = [];
        for (var key in locations) {
          colors.push(scope.randomColor());
        }

        //Creating Ordinal of colors for locations to fetch it like color(location)
        ordinals.color = d3.scaleOrdinal().domain(locations)
          .range(colors);
      };

      /*
       * Calculating height on the basis of names available.
       */
      scope.calculateHeight = function () {
        dimensions.height = dimensions.gridSize * domains.y.length;
      };
      
      /*
       * To set Dimension of main SVG (After resizing or redrawing of Graph)
       */
      scope.setDimensions = function () {
        svg.attr('width', dimensions.width + padding.left + padding.right);
        svg.attr('height', dimensions.height + padding.top + padding.bottom);
      };

      /**
        * Printing Y Axis Labels
        */
      scope.printXAxis = function () {
          
        gContainer.selectAll(".nameLabel").remove();
        var nameLabels = gContainer.selectAll(".nameLabel")
          .data(domains.y)
          .enter().append("g")
            .attr("transform", function(d, i) {
                return "translate(-6, " + (i * dimensions.gridSize) + ")";
              })
            .attr("class", "nameLabel")
            .append("text")
            .text(function (d) { return d.length < 15 ? d : d.substring(0, 15) + '...'; })
            .style("text-anchor", "end")
            .style("font-size", "9px")
            .style("opacity", "0")
            .attr("transform", "translate(-6," + dimensions.gridSize / 1.5 + ")")
            .attr("class", "nameLabel");
    
        nameLabels.transition().duration(1000)
            .style("opacity", "1");
      };

      /**
        * Printing X Axis Labels
        */
      scope.printYAxis = function() {
        
        gContainer.selectAll(".timeLabel").remove();
        var timeLabels = gContainer.selectAll(".timeLabel")
          .data(domains.x)
          .enter().append("g")
            .attr("transform", function(d, i) {
                return "translate(" + (i * dimensions.gridSize + dimensions.gridSize / 2) + ", -6)";
              })
            .attr("class", "timeLabel")
            .append("text")
              .text(function(d) { return d; })
              .attr("y", 0)
              .style("text-anchor", "start")
              .attr("dy", "-1em")
              .style("font-size", "9px")
              .style("opacity", "0")
              .attr("transform", function(d, i) {
                  return "rotate(-45)";
              });
              
        timeLabels.transition().duration(1000)
            .style("opacity", "1");
      };

      //Function to create Grids with events for showing/hiding of tooltips
      scope.printGrids = function () {
        /**
          * Time for Heat Map
          */
        gContainer.selectAll(".hour").remove();
        var cards = gContainer.selectAll(".hour")
          .data(scope.data)
          .enter().append("rect")
            .attr("x", function(d) { return domains.x.indexOf(d.timestamp) * dimensions.gridSize + dimensions.gridSize / 2; })
            .attr("y", function(d) { return domains.y.indexOf(d.name) * dimensions.gridSize + dimensions.gridSize / 2; })
            .attr("rx", 4)
            .attr("ry", 4)
            .attr("class", "hour bordered")
            .style("fill", function(d, i) {
              return '#dddddd';
            })
            .style("opacity", "0");

          cards.transition().duration(2000)
            .style("fill", function(d) { return ordinals.color(d.location); })
            .style("opacity", "1")
            .attr("x", function(d) { return domains.x.indexOf(d.timestamp) * dimensions.gridSize; })
            .attr("y", function(d) { return domains.y.indexOf(d.name) * dimensions.gridSize; })
            .attr("width", dimensions.gridSize)
            .attr("height", dimensions.gridSize);

          cards.on('mouseover', function(d) {
            d3.select(this).style("stroke-width", 4);
            d3.select(this).style("cursor", "pointer");
          
            var tooltip_html = '';
            tooltip_html += '<div><strong>Name: </strong>' + d.name + '</div>';
            tooltip_html += '<div><strong>Location: </strong>' + (d.location) + '</div>';
            tooltip_html += '<div><strong>Time: </strong> ' + d.timestamp + '</div>';

            var x = (domains.x.indexOf(d.timestamp) + 1) * dimensions.gridSize + padding.left;
            var y = (domains.y.indexOf(d.name) + 1) * dimensions.gridSize  + padding.top;
          
            if(dimensions.width - x < 100)
              x -= 200;
            

            // Show tooltip
            tooltip.html(tooltip_html)
              .style('left', x + 'px')
              .style('top', y + 'px')
              .transition()
              .duration(tooltipOptions.transition_duration)
              .style('opacity', 1);
          }).on('mouseout', function () {
            scope.hideTooltip();
            d3.select(this).style("stroke-width", 2);
            d3.select(this).style("cursor", "default");
          });
      };

      scope.drawChart = function () {
        scope.calculateHeight();
        
        scope.setDimensions();
        
        scope.printXAxis();  
        scope.printYAxis();

        scope.printGrids();  
      };

      /**
       * Helper function to generate random color
       */
      scope.randomColor = function () {
        return 'rgb(' + (Math.floor(Math.random() * 80) + 170) + ',' + (Math.floor(Math.random() * 100) + 90) + ',' + (Math.floor(Math.random() * 100) + 90) + ')';
      };

      /**
       * Helper function to hide the tooltip
       */
      scope.hideTooltip = function () {
        tooltip.transition()
          .duration(tooltipOptions.transition_duration)
          .style('opacity', 0);
      };
    }
  };
}]);